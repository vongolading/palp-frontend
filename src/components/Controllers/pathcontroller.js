import {path} from 'components/stores.js'

export default {
	setPath({ pathname }) {
		path.update(r => pathname)
	},
	getPath() {
        return path
	}
}
